package chimmai.panupong.lab2;

/**
 * This Java program that accepts one argument: Your school name,
 * the program will check this name and display that it is an university or a college
 * 
 * Author: Panupong Chimmai
 * ID: 613040070-2
 * Sec: 2
 * Date: January 21, 2019
 * 
 **/

class StringAPI {
	public static void main(String[] args) {
		if (args.length == 1) {
			String schoolName = args[0];
			if (schoolName.toLowerCase().contains("university")) {
				System.out.println(schoolName + " is a university");
			} else if (schoolName.toLowerCase().contains("college")) {
				System.out.println(schoolName + " is a college");
			} else {
				System.out.println(schoolName + " is neither a university nor a college");
			}
		} else {
			System.err.println("Usage: StringAPI <school name> ");
		}

	}

}