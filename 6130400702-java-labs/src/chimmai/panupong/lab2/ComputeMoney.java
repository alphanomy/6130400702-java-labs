package chimmai.panupong.lab2;

/**
* This Java program that accepts four arguments: The amount of one thousand baht banknotes,
* five hundred baht banknotes, one hundred baht banknotes and tewnty baht banknotes.
* The program will compute and then display the total amount of money you have.
* 
* Author: Panupong Chimmai
* ID: 613040070-2
* Sec: 2
* Date: January 21, 2019
*
**/

class ComputeMoney {
	public static void main(String[] args) {
		if (args.length == 4) {
			double oneThousand = Double.parseDouble(args[0]);
			double fiveHundred = Double.parseDouble(args[1]);
			double oneHundred = Double.parseDouble(args[2]);
			double twenty = Double.parseDouble(args[3]);
			double total = (oneThousand * 1000) + (fiveHundred * 500) + (oneHundred * 100) + (twenty * 20);
			System.out.println("Total money is " + total + " Baht");
		} else {
			System.err.println("Usage: ComputeMoney <1,000 Baht> <500 Baht> <100 Baht> <20 Baht>");
		}
	}

}
