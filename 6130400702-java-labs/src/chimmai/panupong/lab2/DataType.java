package chimmai.panupong.lab2;

/**
* This Java program not accepts any argument.
* The output of the program is in format
* "My name is " + <my name>
* "My student ID is " + <my student ID>
* <first letter of my name> + <true> + <last 2 digits of my ID> + <last 2 digits of my ID>
* <last 2 digits of my ID> + <last 2 digits and has the floating point of first 2 digits of my ID> + <last 2 digits and has the floating point of first 2 digits of my ID>
* 
* Author: Panupong Chimmai
* ID: 613040070-2
* Sec: 2
* Date: January 21, 2019
*
**/

class DataType {

	public static void main(String[] args) {
		String name = "Panupong Chimmai";
		String id = "6130400702";
		char firstLetter = name.charAt(0);
		boolean trueOrFalse = true;
		int myIDInOctal = 002;
		int myIDInHex = 0x02;
		long myIDInLong = 02L;
		float myIDInFloat = 02.61f;
		double myIDInDouble = 02.61D;

		System.out.println("My name is " + name);
		System.out.println("My student ID is " + id);
		System.out.println(firstLetter + " " + trueOrFalse + " " + myIDInOctal + " " + myIDInHex);
		System.out.println(myIDInLong + " " + myIDInFloat + " " + myIDInDouble);
	}

}
