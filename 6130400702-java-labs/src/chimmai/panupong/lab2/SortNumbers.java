package chimmai.panupong.lab2;

import java.util.Arrays;

/**
 * This Java program that accepts 5 numeric arguments, the program
 * will sort these numbers and display them in ascending order.
 * 
 * Author: Panupong Chimmai 
 * ID: 613040070-2
 * Sec: 2 
 * Date: January 21, 2019
 * 
 **/

class SortNumbers {
	public static void main(String[] args) {
		if (args.length == 5) {
			double[] value = { Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]),
					Double.parseDouble(args[3]), Double.parseDouble(args[4]) };
			Arrays.sort(value);
			String stringOfArray = Arrays.toString(value);
			String result = stringOfArray.substring(1, stringOfArray.length() - 1);
			System.out.println(result);
		} else {
			System.err.println("Usage: SortNumber <number1> <number2> <number3> <number4> <number5>");
		}
	}

}