package chimmai.panupong.lab2;

/**
* This Java program that accepts three arguments: your favorite football player, the
*  football club that the player plays for, and the nationality of that player. The output of
*  the program is in the format
* "My favorite football player is " + <footballer name>
* "His nationality is " + <nationality>
* "He plays for " + <football club>
*
* Author: Panupong Chimmai
* ID: 613040070-2
* Sec: 2
* Date: January 21, 2019
*
**/

class Footballer {
	public static void main(String[] args) {
		if (args.length == 3) {
			String player = args[0];
			String nationality = args[1];
			String club = args[2];
			System.out.println("My favorite football player is " + player);
			System.out.println("His nationality is " + nationality);
			System.out.println("He plays for " + club);
		} else {
			System.err.println("Usage: Footballer <footballer name> <nationality> <club name>");
		}
	}

}
