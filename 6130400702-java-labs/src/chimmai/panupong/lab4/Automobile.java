package chimmai.panupong.lab4;

/**
 * This Java program is a class for creating objects called "Automobile" 
 * in which objects are composed of gasoline, speed, max speed, acceleration, model and color variables.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since Feb 4, 2019
 * ID: 613040070-2
 * Sec: 2
 */

public class Automobile {

	private int gasoline, speed, maxSpeed, acceleration;
	private String model;
	private Color color;
	static int numberOfAutomobile = 0;
	
	/**
	 * Various colors that can be used
	 */
	public enum Color {
		RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE, BLACK;
	}
	
	/**
	 * Objects named "Automobile" that receive parameters
	 * 
	 * @param gasoline car oil
	 * @param speed the speed of the car at that time
	 * @param maxSpeed max speed of the car
	 * @param acceleration acceleration of the car
	 * @param model model of the car
	 * @param color color of the car
	 */
	public Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		numberOfAutomobile++;
	}
	
	/**
	 * Objects named "Automobile" that not receive any parameters
	 * which the value of this object will be set to:
	 * gasoline: 0
	 * speed: 0
	 * maxSpeed: 160
	 * acceleration: 0
	 * model: Automobile
	 * color: White 
	 */
	public Automobile() {
		this.gasoline = 0;
		this.speed = 0;
		this.maxSpeed = 160;
		this.acceleration = 0;
		this.model = "Automobile";
		this.color = Color.WHITE;
		numberOfAutomobile++;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public int getGasoline() {
		return gasoline;
	}

	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}

	public void minusGasoline(int gasoline) {
		this.gasoline -= gasoline;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void plusSpeed(int speed) {
		this.speed += speed;
	}

	public void minusSpeed(int speed) {
		this.speed -= speed;
	}

	public int getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}

	/**
	 * Override 'toString()' to be a format: 
	 * "Automobile [gasoline=<gasoline>,speed=<speed>, 
	 * 				maxSpeed=<maxSpeed>, acceleration=<acceleration>,
	 * 				model=<model>, color=<color>]
	 */
	public String toString() {
		return "Automobile [gasoline=" + gasoline + ", speed=" + speed + ", maxSpeed=" + maxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}

	public static void main(String[] args) {

	}

}
