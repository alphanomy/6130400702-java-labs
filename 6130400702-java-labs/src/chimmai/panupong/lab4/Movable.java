package chimmai.panupong.lab4;

public interface Movable {
	public void accelerate();
	public void breaks();
	public void setSpeed(int speed);
}
