package chimmai.panupong.lab4;

/**
 * This Java program is intended for creating and testing objects named 
 * "ToyotaAuto" and "HondaAuto".
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since Feb 4, 2019
 * ID: 613040070-2
 * Sec: 2
 */

public class TestDrive2 {

	public static void main(String[] args) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");

		System.out.println(car1);
		System.out.println(car2);

		car1.accelerate();
		car2.accelerate();
		car2.accelerate();

		System.out.println(car1);
		System.out.println(car2);

		car1.breaks();
		car1.breaks();
		car2.breaks();

		System.out.println(car1);
		System.out.println(car2);

		car1.refuel();
		car2.refuel();

		System.out.println(car1);
		System.out.println(car2);
		isFaster(car1, car2);
		isFaster(car2, car1);

	}
	
	/**
	 * Is a method for comparing the speed between an objects.
	 * 
	 * @param car1 first object
	 * @param car2 second object
	 */
	public static void isFaster(Object car1, Object car2) {
		if (((Automobile) car1).getSpeed() > ((Automobile) car2).getSpeed()) {
			System.out.println(((Automobile) car1).getModel() + " is faster than " + ((Automobile) car2).getModel());
		} else {
			System.out
					.println(((Automobile) car1).getModel() + " is NOT faster than " + ((Automobile) car2).getModel());
		}

	}
}
