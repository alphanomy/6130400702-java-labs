package chimmai.panupong.lab4;

/**
 * This Java program is a class for creating objects called "HondaAuto" 
 * which is extends from "Automobile"
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since Feb 4, 2019
 * ID: 613040070-2
 * Sec: 2
 */

public class ToyotaAuto extends Automobile implements Movable, Refuelable {

	public static void main(String[] args) {

	}
	
	/**
	 * Objects named "ToyotaAuto" that receive parameters
	 * and set values of speed to 0
	 * and values of gasoline to 100.
	 * 
	 * @param maxSpeed max speed of the car
	 * @param acceleration acceleration of the car
	 * @param model model of the car
	 */
	public ToyotaAuto(int maxSpeed, int acceleration, String model) {
		setMaxSpeed(maxSpeed);
		setAcceleration(acceleration);
		setModel(model);
		setSpeed(0);
		setGasoline(100);
	}
	
	/**
	 * Override method "refuel()"
	 * 
	 * refueling the car by setting the gas to 100
	 */
	public void refuel() {
		System.out.println(getModel() + " refuels");
		setGasoline(100);
	}
	
	/**
	 * Override method "acceleration()"
	 * 
	 * increases the current speed by the acceleration and decreases gasoline by 15. 
	 * the method also displays message "accelerates".
	 */
	public void accelerate() {
		System.out.println(getModel() + " accelerates");
		if ((getSpeed() + getAcceleration()) > getMaxSpeed()) {
			setSpeed(getMaxSpeed());
		} else {
			plusSpeed(getAcceleration());
		}

		if ((getGasoline() - 15) < 0) {
			setGasoline(0);
		} else {
			minusGasoline(15);
		}
	}
	
	/**
	 * Override method "breaks()"
	 * 
	 * decreases the current speed by the acceleration and decreases gasoline by 15.
	 * the method also displays message "brakes".
	 */
	public void breaks() {
		System.out.println(getModel() + " breaks");
		if ((getSpeed() - getAcceleration()) < 0) {
			setSpeed(0);
		} else {
			minusSpeed(getAcceleration());
		}

		if ((getGasoline() - 15) < 0) {
			setGasoline(0);
		} else {
			minusGasoline(15);
		}
	}
	
	/**
	 * Override method "setSpeed(<int speed>)"
	 */
	public void setSpeed(int speed) {
		if (speed > getMaxSpeed()) {
			setSpeed(getMaxSpeed());
		} else if (speed < 0) {
			setSpeed(0);
		}
	}
	
	/**
	 * Override method "toString()" to be a format: 
	 * <model> gas:<gasoline> speed:<speed> max_speed:<maxSpeed> acceleration:<acceleration>
	 */
	public String toString() {
		return getModel() + " gas:" + getGasoline() + " speed:" + getSpeed() + " max_speed:" + getMaxSpeed()
				+ " acceleration:" + getAcceleration();
	}
}
