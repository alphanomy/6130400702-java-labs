package chimmai.panupong.lab3;

import java.util.Arrays;
import java.lang.Math;

/**
 * This Java program that accepts arguments in format: <numNumbers> <numbers> ...
 * The first argument is how many numbers to be entered. The rest of the program arguments are list of numbers.
 * The program will find the minimum, the maximum, the average and the standard deviation 
 * of the list of numbers entered.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since Feb 3, 2019
 * ID: 613040070-2
 * Sec: 2
 */

public class BasicStat {
	
	static double[] num;
	static int numInput;

	public static void main(String[] args) {
		acceptInput(args);
		displayStats();
	}

	/**
	 * Split the values into two values: the number of numbers 
	 * and the array of numbers.
	 * 
	 * @param args arguments that user entered into numbers
	 */
	public static void acceptInput(String[] args) {
		numInput = Integer.parseInt(args[0]);
		double[] array = new double[numInput];
		
		if (numInput != args.length - 1) {
			System.err.println("<BasicStat> <numNumbers> <numbers>...");
			System.exit(0);
		}
		
		for (int i = 1; i < args.length; i++) {
			array[i - 1] = Double.parseDouble(args[i]);
		}
		num = array;
	}

	/**
	 * Calculate to find the maximum value, the minimum value,
	 * the average and the standard deviation of the array
	 * the display it.
	 */
	public static void displayStats() {
		double[] array = new double[numInput];
		double numberInput = numInput;
		double sumNum, forCalculate, mu, deviation;
		
		/**
		 * Arrange the array from ascending
		 */
		array = num;
		Arrays.sort(array);
		System.out.println("Max is " + array[array.length - 1] + " Min is " + array[0]);
		
		//find sumNum where is the summation of the array
		sumNum = 0;
		for (int i = 0; i < array.length; i++) {
			sumNum += array[i];
		}
		
		mu = (1 / numberInput) * sumNum; //find mu where is the value for calculating standard deviation

		//forCalculate just a variable to make it easy to calculate
		forCalculate = 0;
		for (int i = 0; i < array.length; i++) {
			forCalculate += Math.pow((array[i] - mu), 2);
		}
		
		deviation = Math.sqrt((1 / numberInput) * forCalculate);

		System.out.println("Average is " + (sumNum / numberInput));
		System.out.println("Standard Deviation is " + deviation);
	}

}
