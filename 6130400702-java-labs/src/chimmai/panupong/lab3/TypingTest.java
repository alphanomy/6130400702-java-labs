package chimmai.panupong.lab3;

import java.lang.System;
import java.util.Arrays;
import java.util.Scanner;

/**
 * TypingTest is a program that test whether a user types faster or slower than an average person. 
 * The program randoms 8 colors from a list of rainbow colors (RED, ORANGE, YELLOW, GREEN, BLUE, 
 * INDIGO, VIOLET). A user must type in the same list ignoring case. If the user type in incorrectly,
 * he needs to type in again. After the correct input is enter, the time used for typing in will be calculated 
 * and if he types in faster than or equal to 12 seconds, he types faster than average otherwise 
 * he types slower than average.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since Feb 3, 2019
 * ID: 613040070-2
 * Sec: 2
 * 
 */

public class TypingTest {

	public static void main(String[] args) {
		String answer, colorsInString;
		String[] colors = new String[8];
		
		/**
		 * Random 8 colors using numbers from 0 - 6
		 * and keep the colors in the array.
		 */
		for (int i = 0; i < colors.length; i++) {
			int num;
			num = (int) (Math.random() * 8);
			if (num == 0) {
				colors[i] = "RED";
			} else if (num == 1) {
				colors[i] = "ORANGE";
			} else if (num == 2) {
				colors[i] = "YELLOW";
			} else if (num == 3) {
				colors[i] = "GREEN";
			} else if (num == 4) {
				colors[i] = "BLUE";
			} else if (num == 5) {
				colors[i] = "INDIGO";
			} else {
				colors[i] = "VIOLET";
			}
		}
		
		/**
		 * Convert array of colors to String and display its.
		 */
		colorsInString = Arrays.toString(colors).replaceAll(", ", " ");
		colorsInString = colorsInString.substring(1, colorsInString.length() - 1);
		System.out.print(colorsInString + "\n");
		
		/**
		 * Take the current time that start typing 
		 * and add to variable. 
		 */
		double timeBefore, timeAfter, useTime;
		timeBefore = System.currentTimeMillis();
		
		/**
		 * Get the input and check whether it matches the string of color shown or not,
		 * if yes, take the current time add to other variable then calculate the time spent in typing
		 * and show summary results, if not, start retyping but time still continues.
		 */
		while (true) {
			Scanner scanner = new Scanner(System.in);
			System.out.print("Type you answer: ");
			answer = scanner.nextLine();
			if (answer.equalsIgnoreCase(colorsInString)) {
				timeAfter = System.currentTimeMillis();
				useTime = (timeAfter - timeBefore) * 0.001;
				System.out.println("Your time is " + useTime + " seconds.");
				scanner.close();
				
				if (useTime <= 12) {
					System.out.println("Tou type faster than average person");
					System.exit(0);
				} else {
					System.out.println("Tou type slower than average person");
					System.exit(0);
				}
				
			}
			
		}

	}

}
