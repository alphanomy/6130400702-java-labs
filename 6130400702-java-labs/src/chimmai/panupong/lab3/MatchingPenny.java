package chimmai.panupong.lab3;

import java.util.Scanner;
import java.lang.Math;

/**
 * This java program that simulates a coin tossing game called matching penny. 
 * The program reads in a choice (Head or Tail (ignore case)) from a user then compares that choice 
 * with a randomly generated choice from a computer. If the choices are the same, 
 * the user wins. If the choices are different, the computer wins. 
 * And if want to exit the program, type "exit".
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since Jan 28, 2019
 * ID: 613040070-2
 * Sec: 2
 * 
 */

public class MatchingPenny {
	public static void main(String[] args) {
		String guess, computer;
		int num;
		Scanner scanner = new Scanner(System.in);
		
		while (true) {
			System.out.print("Enter head or tail: ");
			guess = scanner.nextLine().toLowerCase();
			
			if (guess.equals("exit")) {
				System.out.println("Good bye!");
				scanner.close();
				System.exit(0);
			}
			
			if (guess.equals("head") || guess.equals("tail")) {
				System.out.println("You play " + guess);
				num = (int)(Math.random() * 2);
				if (num == 0) {
					computer = "head";
				} else {
					computer = "tail";
				}
				
				System.out.println("Computer play " + computer);
				if (guess.equals(computer)) {
					System.out.println("You win.");
				} else {
					System.out.println("Computer win.");
				}
				
			} else {
				System.err.print("Incorrect input. head and tail only\n");
			}
		}
	}

}
