package chimmai.panupong.lab3;

import java.util.Scanner;
import java.lang.Math;

/**
 * This java program that simulates a coin tossing game called matching penny. 
 * The program reads in a choice (Head or Tail (ignore case)) from a user then compares that choice 
 * with a randomly generated choice from a computer. If the choices are the same, 
 * the user wins. If the choices are different, the computer wins. 
 * And if want to exit the program, type "exit".
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since Feb 3, 2019
 * ID: 613040070-2
 * Sec: 2
 * 
 */

public class MatchingPennyMethod {

	static String humanChoice;
	static String comChoice;

	public static void main(String[] args) {
		while (true) {
			MatchingPennyMethod game = new MatchingPennyMethod();
			game.acceptInput();
			game.genComChoice();
			System.out.println("You play " + humanChoice);
			System.out.println("Computer play " + comChoice);
			displayWiner(humanChoice, comChoice);
		}
	}
	
	/**
	 * Get input, check the accuracy.
	 * 
	 * @return player choice
	 */
	public String acceptInput() {
		Scanner scanner = new Scanner(System.in);

		while (true) {
			System.out.print("Enter head or tail: ");
			humanChoice = scanner.nextLine().toLowerCase();

			if (humanChoice.equals("exit")) {
				System.out.println("Good bye!");
				scanner.close();
				System.exit(0);
			} else if (humanChoice.equals("head") || humanChoice.equals("tail")) {
				return humanChoice;
			} else {
				System.err.print("Incorrect input. head and tail only\n");
			}
			
		}
	}

	/**
	 * Randomly generate computer choice.
	 * 
	 * @return computer choice
	 */
	public String genComChoice() {
		int num;
		num = (int) (Math.random() * 2);
		if (num == 0) {
			comChoice = "head";
			return comChoice;
		} else {
			comChoice = "tail";
			return comChoice;
		}
	}
	
	/**
	 * Compare between player choice and computer choice
	 * then display the winner.
	 */
	public static void displayWiner(String humanChoice, String comChoice) {
		if (humanChoice.equals(comChoice)) {
			System.out.println("You win.");
		} else {
			System.out.println("Computer win.");
		}

	}

}