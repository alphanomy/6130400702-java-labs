package chimmai.panupong.lab11;

import java.awt.event.ActionEvent;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import chimmai.panupong.lab5.MobileDevice;

/**
 * This java program MobileDeviceFormV12 is a program to display a mobile device form.
 * The program extends from MobileDeviceFormV111.
 * Which this program is for filling in various device information 
 * which this program can save the information entered on the file 
 * and save the file to the computer of the user and can also view files that users 
 * have saved from this program.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-05-05
 * ID: 613040070-2
 * Sec: 2
 */

public class MobileDeviceFormV12 extends MobileDeviceFormV11 {

	private static final long serialVersionUID = -5129670354539476506L;

	public MobileDeviceFormV12(String title) {
		super(title);
	}
	
	public void WriteObjectToFile() {
		 
        try {
            FileOutputStream fileOut = new FileOutputStream(fc.getSelectedFile(), true);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            for (MobileDevice dev : devicesList) {
            	 objectOut.writeObject(dev);
    		}
            fileOut.close();
            objectOut.close();

		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
    }
	
	public void ReadObjectFromFile() {
		String readFile = "";
		
		try {
            FileInputStream fileIn = new FileInputStream(fc.getSelectedFile());
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            
            try {
	            while (true) {
	            	MobileDevice dev = (MobileDevice) objectIn.readObject();
	            	readFile += dev.toString() + "\n";
	            }
            } catch (EOFException e) {
    			JOptionPane.showMessageDialog(null, readFile);
    			objectIn.close();
            }
        	
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "File not found");
		} catch (IOException e) {
			e.printStackTrace(System.err);
        } catch (ClassNotFoundException e) {
			e.printStackTrace(System.err);
		}
	}
	
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		
		if (src == saveMI) {
			WriteObjectToFile();
		} else if (src == openMI) {
			ReadObjectFromFile();
		}
		
	}
	
	public static void createAndShowGUI(){
		MobileDeviceFormV12 mobileDeviceForm12 = 
				new MobileDeviceFormV12("Mobile Device Form V12");
		mobileDeviceForm12.addComponents();
		mobileDeviceForm12.addMenus();
		mobileDeviceForm12.setFrameFeatures();
		mobileDeviceForm12.addListeners();
		mobileDeviceForm12.addKeyEvents();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
