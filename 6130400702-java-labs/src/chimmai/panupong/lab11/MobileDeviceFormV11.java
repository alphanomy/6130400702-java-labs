package chimmai.panupong.lab11;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV11 is a program to display a mobile device form.
 * The program extends from MobileDeviceFormV10.
 * Which this program is for filling in various device information 
 * which this program can check whether the information is correct or not.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-05-05
 * ID: 613040070-2
 * Sec: 2
 */

public class MobileDeviceFormV11 extends MobileDeviceFormV10 {
	private static final long serialVersionUID = -7046292339066308977L;
	protected static final int MIN_WEIGHT = 100;
	protected static final int MAX_WEIGHT = 3000;
	
	public MobileDeviceFormV11(String title) {
			super(title);
		}
	
	public void handleOKButton() {
		boolean error = false;
		if (mNameTxtField.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Please enter model name");
			error = true;
		}
		
		try 
		{
			double weightInput = Double.parseDouble(weightTxtField.getText());
			if (weightInput > MAX_WEIGHT) {
				JOptionPane.showMessageDialog(null,
						"Too heavy: valid weight is [" + MIN_WEIGHT + "," + MAX_WEIGHT + "]");
				error = true;
			} else if (weightInput < MIN_WEIGHT) {
				JOptionPane.showMessageDialog(null,
						"Too light: valid weight is [" + MIN_WEIGHT + "," + MAX_WEIGHT + "]");
				error = true;
			}
		}
		catch (NumberFormatException e)
		{
			JOptionPane.showMessageDialog(null, "Please enter only numeric input for weight");
			error = true;
		}
		
		if (!error) {
			super.handleOKButton();
		}
	}
	
	public static void createAndShowGUI(){
		MobileDeviceFormV11 mobileDeviceForm11 = 
				new MobileDeviceFormV11("Mobile Device Form V11");
		mobileDeviceForm11.addComponents();
		mobileDeviceForm11.addMenus();
		mobileDeviceForm11.setFrameFeatures();
		mobileDeviceForm11.addListeners();
		mobileDeviceForm11.addKeyEvents();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
