package chimmai.panupong.lab11;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import chimmai.panupong.lab10.MobileDeviceFormV9;
import chimmai.panupong.lab5.MobileDevice;

/**
 * This java program MobileDeviceFormV10 is a program to display a mobile device form.
 * The program extends from MobileDeviceFormV9.
 * Which this program is for filling in various device information which can be 
 * displayed, sorted, searched, and removed the informations that user have entered.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-05-05
 * ID: 613040070-2
 * Sec: 2
 */

public class MobileDeviceFormV10 extends MobileDeviceFormV9 {
	private static final long serialVersionUID = 5732078424313974861L;
	protected static ArrayList<MobileDevice> devicesList = new ArrayList<MobileDevice>();
	protected JMenu dataMenu;
	protected JMenuItem displayMI, sortMI, searchMI, removeMI;

	public MobileDeviceFormV10(String title) {
		super(title);
	}
	
	protected void addMenus() {
		super.addMenus();
		dataMenu = new JMenu("Data");
		displayMI = new JMenuItem("Display");
		sortMI = new JMenuItem("Sort");
		searchMI = new JMenuItem("Search");
		removeMI = new JMenuItem("Remove");
		
		menuBar.add(dataMenu);
		dataMenu.add(displayMI);
		dataMenu.add(sortMI);
		dataMenu.add(searchMI);
		dataMenu.add(removeMI);
		
	}
	
	protected void addMobileDevice() {
		MobileDevice inputNewDevice = new MobileDevice(mNameTxtField.getText(), 
				os, Integer.parseInt(priceTxtField.getText()),
				Double.parseDouble(weightTxtField.getText()));
		
		devicesList.add(inputNewDevice);
		System.out.println(devicesList);
	}
	
	protected void displayMobileDevices() {
		String displayDev = "";
		for (int i = 0; i < devicesList.size(); i++) {
			displayDev += (i + 1) + ": " + devicesList.get(i) + "\n\n";
		}
		JOptionPane.showMessageDialog(null, displayDev);
	}
	
	protected void sortMobileDevices() {
		Collections.sort(devicesList, new PriceComparator());
		displayMobileDevices();
	}
	
	protected void searchMobileDevices() {
		String search = JOptionPane.showInputDialog("Please input model name to search:");
		Boolean found = false;
		for(MobileDevice dev : devicesList){
	        if(dev.getModeName() != null && dev.getModeName().contains(search)) {
	        	JOptionPane.showMessageDialog(null, dev.toString() + "\nis found");
	        	found = true;
	        }
		}
		if (found == false) {
			JOptionPane.showMessageDialog(null, search +  " is NOT found");
		}
	}

	protected void removeMobileDevices() {
		String remove = JOptionPane.showInputDialog("Please input model name to search:");
		Boolean found = false;
		for(MobileDevice dev : devicesList){
	        if(dev.getModeName() != null && dev.getModeName().contains(remove)) {
	        	devicesList.remove(0);
	        	JOptionPane.showMessageDialog(null, dev.toString() + "\nis removed");
	        	found = true;
	        	break;
	        }
		}
		if (found == false) {
			JOptionPane.showMessageDialog(null, remove +  " is NOT found");
		}
	}
	
	public void handleOKButton() {
		super.handleOKButton();
		addMobileDevice();
	}
	
	protected void addListeners() {
		super.addListeners();
		displayMI.addActionListener(this);
		sortMI.addActionListener(this);
		searchMI.addActionListener(this);
		removeMI.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		
		if (src == okButton) {
			handleOKButton();
		} else if (src == displayMI) {
			displayMobileDevices();
		} else if (src == sortMI) {
			sortMobileDevices();
		} else if (src == searchMI) {
			searchMobileDevices();
		} else if (src == removeMI) {
			removeMobileDevices();
		} else {
			super.actionPerformed(event);
		}
	}
	public static void createAndShowGUI(){
		MobileDeviceFormV10 mobileDeviceForm10 = 
				new MobileDeviceFormV10("Mobile Device Form V10");
		mobileDeviceForm10.addComponents();
		mobileDeviceForm10.addMenus();
		mobileDeviceForm10.setFrameFeatures();
		mobileDeviceForm10.addListeners();
		mobileDeviceForm10.addKeyEvents();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

class PriceComparator implements Comparator<MobileDevice> {
    @Override
    public int compare(MobileDevice obj1, MobileDevice obj2) {
        Integer p1 = ((MobileDevice) obj1).getPrice();
        Integer p2 = ((MobileDevice) obj2).getPrice();

        if (p1 > p2) {
            return 1;
        } else if (p1 < p2){
            return -1;
        } else {
            return 0;
        }
     }
}
