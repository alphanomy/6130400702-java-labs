package chimmai.panupong.lab6;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This java program MySimpleWindow is a program to display a simple GUI.
 * This program will display an OK button and a Cancel button.
 * 
 * @author Panupong Chimmai
 * ID: 613040070-2 
 * Sec: 2
 * @since 2019-03-03
 * @version 1.0
 * 
 */

public class MySimpleWindow extends JFrame {
	protected JPanel windowPanel, buttonsPanel;
	protected JButton okButton;
	protected JButton cancelButton;
	
	
	public MySimpleWindow(String title) {
		super(title);
	}
	
	protected void addComponents() {
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		buttonsPanel = new JPanel();
		
		FlowLayout layout = new FlowLayout();
		layout.setHgap(20);
		layout.setVgap(10);
		buttonsPanel.setLayout(layout);
		buttonsPanel.add(cancelButton);
		buttonsPanel.add(okButton);
		setLayout(new BorderLayout());
		add(buttonsPanel, BorderLayout.SOUTH);	
	}
	
	protected void setFrameFeatures() {
		this.setLocationRelativeTo(null);
		pack();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
