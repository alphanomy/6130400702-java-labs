package chimmai.panupong.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV3 is a program to display a simple GUI.
 * The program will display like MobileDeviceFormV2.
 * But adding a message showing the features of the device
 * which is displayed under the "Type" between "Type" and "Review".
 * And also added a menu bar that includes file and config menus.
 *
 * @author Panupong Chimmai
 * ID: 613040070-2 
 * Sec: 2
 * @since 2019-03-03
 * @version 1.0
 * 
 */

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	protected JMenuBar menuBar;
	protected JMenu fileMenu, confMenu;
	protected JMenuItem newMI, openMI, saveMI, exitMI;
	protected JMenuItem colorMI, sizeMI;
	protected JPanel featPanel;
	protected JList featureList;	
	protected JLabel featLabel;
	
	public MobileDeviceFormV3(String title) {
		super(title);
	}
	
	protected void addMenus() {
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		confMenu = new JMenu("Config");
		newMI = new JMenuItem("New");
		openMI = new JMenuItem("Open");
		saveMI = new JMenuItem("Save");
		exitMI = new JMenuItem("Exit");
		colorMI = new JMenuItem("Color");
		sizeMI = new JMenuItem("Size");
		
		menuBar.add(fileMenu);
		menuBar.add(confMenu);
		fileMenu.add(newMI);
		fileMenu.add(openMI);
		fileMenu.add(saveMI);
		fileMenu.add(exitMI);
		confMenu.add(colorMI);
		confMenu.add(sizeMI);
		
		this.setJMenuBar(menuBar);
	}
	
	protected void addComponents() {
		super.addComponents();
		featLabel = new JLabel("Features:");
		String features[] = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };
		featureList = new JList(features);
		
		featPanel = new JPanel();
		featPanel.setLayout(new GridLayout(1, 1));
		featPanel.add(featLabel);
		featPanel.add(featureList);
		
		overallPanel.add(featPanel, BorderLayout.CENTER);
	
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceForm3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceForm3.addComponents();
		mobileDeviceForm3.addMenus();
		mobileDeviceForm3.setFrameFeatures();
		}

		public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}

	}