package chimmai.panupong.lab6;

import java.awt.*;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV2 is a program to display a simple GUI.
 * The program will display like MobileDeviceFormV1.
 * Only adding a box for selecting the type of device 
 * and the device review message box.
 * Which is displayed under the "Mobile OS"
 *
 * @author Panupong Chimmai
 * ID: 613040070-2 
 * Sec: 2
 * @since 2019-03-03
 * @version 1.0
 * 
 */

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	protected JLabel typeLabel, revLabel;
	protected JComboBox typeChoice;
	protected JPanel typePanel, reviewPanel, overallPanel;
	protected JTextArea revTxtArea;
	protected JScrollPane revScroll;
	
	public MobileDeviceFormV2(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();
		
		typeLabel = new JLabel("Type:");
		typeChoice = new JComboBox();
		typeChoice.addItem("Phone");
		typeChoice.addItem("Tablet");
		typeChoice.addItem("Smart TV");
		typeChoice.setEditable(false);
		
		typePanel = new JPanel();
		typePanel.setLayout(new GridLayout(1,1));
		typePanel.add(typeLabel);
		typePanel.add(typeChoice);
		
		
		revLabel = new JLabel("Review:");
		revTxtArea = new JTextArea(3, 35);
		revTxtArea.setLineWrap(true);
		revTxtArea.setWrapStyleWord(true);
		revTxtArea.setText("Bigger than previous Note phones in every way, "
				+ "the Samsung Galaxy Note 9 has a larger 6.4-inch screen, "
				+ "heftier 4,000mAh battery, and a massive 1TB of storage option.");
		revTxtArea.setRows(2);
		
		revScroll = new JScrollPane(revTxtArea);
		revScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		reviewPanel = new JPanel();
		reviewPanel.setLayout(new BorderLayout());
		reviewPanel.add(revLabel, BorderLayout.NORTH);
		reviewPanel.add(revScroll, BorderLayout.SOUTH);
	
		overallPanel = new JPanel();
		overallPanel.setLayout(new BorderLayout());
		overallPanel.add(typePanel, BorderLayout.NORTH);
		overallPanel.add(reviewPanel, BorderLayout.SOUTH);
		
		add(overallPanel, BorderLayout.CENTER);
	}
	
	public static void createAndShowGUI() {
	MobileDeviceFormV2 mobileDeviceForm2 = new MobileDeviceFormV2("Mobile Device Form V2");
	mobileDeviceForm2.addComponents();
	mobileDeviceForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}