package chimmai.panupong.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV1 is a program to display a simple GUI.
 * The program will display a about the filtering of mobile device information.
 * Which includes brand name, model name, weight, price and OS, 
 * each of which has a space to fill in information.
 * And also have an OK button and a Cancel button.
 * 
 * @author Panupong Chimmai
 * ID: 613040070-2 
 * Sec: 2
 * @since 2019-03-03
 * @version 1.0
 * 
 */

public class MobileDeviceFormV1 extends MySimpleWindow {
	protected JPanel infoPanel, radioButtPanel;
	protected JLabel brandLabel, modelLabel, weightLabel, priceLabel, osLabel;
	protected JTextField brandTxtField, modelTxtField, weightTxtField, priceTxtField;
	protected JRadioButton androidCheckBox, iOSCheckBox;
	protected ButtonGroup buttonGroup;
	
	public MobileDeviceFormV1(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();
		infoPanel = new JPanel();
		radioButtPanel = new JPanel();
		brandLabel = new JLabel("Brand Name:");
		modelLabel = new JLabel("Model Name:");
		weightLabel = new JLabel("Weight (kg.):");
		priceLabel = new JLabel("Price (Baht):");
		osLabel = new JLabel("Mobile OS:");
		brandTxtField = new JTextField(15);
		modelTxtField = new JTextField(15);
		weightTxtField = new JTextField(15);
		priceTxtField = new JTextField(15);
		androidCheckBox = new JRadioButton("Android");
		iOSCheckBox = new JRadioButton("iOS");
		
		buttonGroup = new ButtonGroup();
		radioButtPanel.add(androidCheckBox);
		radioButtPanel.add(iOSCheckBox);
		buttonGroup.add(androidCheckBox);
		buttonGroup.add(iOSCheckBox);
		
		
		infoPanel.setLayout(new GridLayout(5,2));
		infoPanel.add(brandLabel);
		infoPanel.add(brandTxtField);
		infoPanel.add(modelLabel);
		infoPanel.add(modelTxtField);
		infoPanel.add(weightLabel);
		infoPanel.add(weightTxtField);
		infoPanel.add(priceLabel);
		infoPanel.add(priceTxtField);
		infoPanel.add(osLabel);
		infoPanel.add(radioButtPanel);
		
		add(infoPanel, BorderLayout.NORTH);
	}	

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceForm1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceForm1.addComponents();
		mobileDeviceForm1.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}