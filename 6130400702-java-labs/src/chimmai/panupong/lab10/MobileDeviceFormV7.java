package chimmai.panupong.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import chimmai.panupong.lab7.MobileDeviceFormV6;

/**
 * This java program MobileDeviceFormV7 is a program to display a mobile device GUI.
 * The program extends from MobileDeviceFormV6 so this will 
 * display like MobileDeviceFormV6.
 * But only adding a handler of each button.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-21
 * ID: 613040070-2
 * Sec: 2
 */

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener, ItemListener {
	private static final long serialVersionUID = -7121585868328596230L;
	protected static String os, featsString;

	public MobileDeviceFormV7(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		android.setSelected(true);
		features.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		features.setSelectedIndices(new int[]{0, 1, 3});
		
		android.setActionCommand("Android");
		iOS.setActionCommand("iOS");
	}
	
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		String action = event.getActionCommand();

		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		} else if (action.equals("Android")) {
			JOptionPane.showMessageDialog(null, "Your OS platform is now changed to Android");
			os = "Android";
		} else if (action.equals("iOS")) {
			JOptionPane.showMessageDialog(null, "Your OS platform is now changed to iOS");
			os = "iOS";
		} else {

		}
	}
	
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			JOptionPane.showMessageDialog(null, "Type is updated to " + typeBox.getSelectedItem());
		}
		
	}
	
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		android.addActionListener(this);
		iOS.addActionListener(this);
		
		typeBox.addItemListener(this);
		features.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
            	boolean isAdjusting = e.getValueIsAdjusting(); 
            	if (!isAdjusting) {
	        		featsString = String.join(", ", features.getSelectedValuesList());
	        		JOptionPane.showMessageDialog(null, featsString);
            	}
            }
        });
	}
	
	public void handleOKButton() {
		if (os == null) {
			os = "Android";
			
		} if (featsString == null) {
			featsString = String.join(", ",features.getSelectedValuesList());
		}
		
		JOptionPane.showMessageDialog(null, "Brand Name: " + bNameTxtField.getText() 
		+ ", Model Name: " + mNameTxtField.getText() + ", Weight: " + weightTxtField.getText() 
		+ ", Price: " + priceTxtField.getText() + "\nOS: " + os + "\nType: " + typesValues[typeBox.getSelectedIndex()] 
		+ "\nFeatures: " + featsString + "\nReview: " + reviewTxtArea.getText());
	}
	public void handleCancelButton() {
		bNameTxtField.setText("");
		mNameTxtField.setText("");
		weightTxtField.setText("");
		priceTxtField.setText("");
		reviewTxtArea.setText("");
	}
	
	
	public static void createAndShowGUI(){
		MobileDeviceFormV7 mobileDeviceForm7 = 
				new MobileDeviceFormV7("Mobile Device Form V7");
		mobileDeviceForm7.addComponents();
		mobileDeviceForm7.addMenus();
		mobileDeviceForm7.setFrameFeatures();
		mobileDeviceForm7.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}


