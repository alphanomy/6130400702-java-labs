package chimmai.panupong.lab10;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV8 is a program to display a mobile device GUI.
 * The program extends from MobileDeviceFormV7 so this will 
 * display like MobileDeviceFormV7.
 * But only adding a shortcut keys into menu tabs and adding custom menu in config menu tab.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-21
 * ID: 613040070-2
 * Sec: 2
 */

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {
	
	private static final long serialVersionUID = 2847870317627304665L;
	protected JMenuItem customMI;

	public MobileDeviceFormV8(String title) {
		super(title);
	}
	
	protected void addMnemonicKeys() {
		fileMenu.setMnemonic(KeyEvent.VK_F);
		newMI.setMnemonic(KeyEvent.VK_N);
		openMI.setMnemonic(KeyEvent.VK_O);
		saveMI.setMnemonic(KeyEvent.VK_S);
		exitMI.setMnemonic(KeyEvent.VK_X);
		configMenu.setMnemonic(KeyEvent.VK_C);
		colorMenu.setMnemonic(KeyEvent.VK_L);
		blueMI.setMnemonic(KeyEvent.VK_B);
		greenMI.setMnemonic(KeyEvent.VK_G);
		redMI.setMnemonic(KeyEvent.VK_R);
		customMI.setMnemonic(KeyEvent.VK_U);	
	}
	
	@SuppressWarnings("deprecation")
	protected void addAcceleratorKeys() {
		newMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		openMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		saveMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		exitMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		blueMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		greenMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		redMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		customMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
	}
	
	protected void addKeyEvents() {
		addMnemonicKeys();
		addAcceleratorKeys();
	}
	
	protected void addSubMenus() {
		super.addSubMenus();
		customMI = new JMenuItem("Custom ...");
		colorMenu.add(customMI);
	}
	
	public static void createAndShowGUI(){
		MobileDeviceFormV8 mobileDeviceForm8 = 
				new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceForm8.addComponents();
		mobileDeviceForm8.addMenus();
		mobileDeviceForm8.setFrameFeatures();
		mobileDeviceForm8.addListeners();
		mobileDeviceForm8.addKeyEvents();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
