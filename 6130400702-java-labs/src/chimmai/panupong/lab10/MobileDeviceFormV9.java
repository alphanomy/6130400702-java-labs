package chimmai.panupong.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV9 is a program to display a mobile device GUI.
 * The program extends from MobileDeviceFormV8 so this will 
 * display like MobileDeviceFormV8.
 * But adding a handler of open, save, and exit menu.
 * And also adding a handler of color config menu.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-21
 * ID: 613040070-2
 * Sec: 2
 */

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {
	private static final long serialVersionUID = 8478170488738614884L;
	protected JFileChooser fc;
    
	public MobileDeviceFormV9(String title) {
		super(title);
	}
	
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
	    fc = new JFileChooser();
	    
	    if (event.getSource() == openMI) {
            int returnVal = fc.showOpenDialog(this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                JOptionPane.showMessageDialog(null, "Opening file " + file.getName());
            } else {
            	JOptionPane.showMessageDialog(null, "Open command cancelled by user");
            }

        } else if (event.getSource() == saveMI) {
            int returnVal = fc.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                JOptionPane.showMessageDialog(null, "Saving file " + file.getName());
            } else {
                JOptionPane.showMessageDialog(null, "Save command cancelled by user");
            }
            
        } else if (event.getSource() == exitMI) {
        	System.exit(0);
        	
        } else if (event.getSource() == redMI) {
        	reviewTxtArea.setBackground(Color.RED);
        	
        } else if (event.getSource() == greenMI) {
        	reviewTxtArea.setBackground(Color.GREEN);
        	
        } else if (event.getSource() == blueMI) {
        	reviewTxtArea.setBackground(Color.BLUE);
        	
        } else if (event.getSource() == customMI) {
        	Color newColor = JColorChooser.showDialog(null, "Choose Color", reviewTxtArea.getBackground());
        	
        	if(newColor != null){
        		reviewTxtArea.setBackground(newColor);
            }
        }
    }
	
	protected void addListeners() {
		super.addListeners();
		openMI.addActionListener(this);
		saveMI.addActionListener(this);
		exitMI.addActionListener(this);
		redMI.addActionListener(this);
		greenMI.addActionListener(this);
		blueMI.addActionListener(this);
		customMI.addActionListener(this);
	}

	
	public static void createAndShowGUI(){
		MobileDeviceFormV9 mobileDeviceForm9 = 
				new MobileDeviceFormV9("Mobile Device Form V9");
		mobileDeviceForm9.addComponents();
		mobileDeviceForm9.addMenus();
		mobileDeviceForm9.setFrameFeatures();
		mobileDeviceForm9.addListeners();
		mobileDeviceForm9.addKeyEvents();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}