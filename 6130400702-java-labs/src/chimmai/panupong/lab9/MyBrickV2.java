package chimmai.panupong.lab9;

import chimmai.panupong.lab8.MyBrick;

/**
 * Is a class that is intended to create 
 * a brick object with a width of 80 units
 * and a height of 20 units
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */


public class MyBrickV2 extends MyBrick {
	protected boolean visible = true;
	
	public MyBrickV2(int x, int y) {
		super(x, y);
	}
}
