package chimmai.panupong.lab9;

import javax.swing.SwingUtilities;

/**
 * Java program that brings the components in class MyCanvasV7
 * to display on the screen.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyFrameV7 extends MyFrameV6 {
	public MyFrameV7(String text) {
		super(text);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV7 msw = new MyFrameV7("My Frame V7");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV7());
	}

}

