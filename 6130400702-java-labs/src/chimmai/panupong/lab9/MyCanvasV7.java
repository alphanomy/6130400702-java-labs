package chimmai.panupong.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import chimmai.panupong.lab8.MyBall;
import chimmai.panupong.lab8.MyBrick;
import chimmai.panupong.lab8.MyCanvas;

/**
 * Is a class that paints a ball and 10 bricks 
 * the ball will move along the screen. If the ball hits 
 * the edge of the screen or brick,
 * that brick will invisible and the ball will bounce back.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvasV7 extends MyCanvasV6 implements Runnable {
	protected int numBricks = MyCanvas.WIDTH/MyBrick.brickWidth;
	protected MyBrickV2[] brick;
	protected Thread running;
	
	public MyCanvasV7() {
		ball = new MyBallV2(0, 0);
		running = new Thread(this);
		ball.ballVelX = 2;
		ball.ballVelY = 2;
		brick = new MyBrickV2[numBricks];
		for (int i = 0; i < numBricks; i++) {
			brick[i] = new MyBrickV2(MyBrick.brickWidth * i, MyCanvasV7.HEIGHT / 2);
		}
		running.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
			
		for (int i = 0; i < numBricks; i++) {
			g2d.setColor(Color.RED);
			g2d.fill(brick[i]);
			
			g2d.setColor(Color.BLUE);
	    	g2d.setStroke(new BasicStroke(3));
	    	g2d.draw(brick[i]);
	    	
	    	if (!brick[i].visible) {
	    		g2d.setColor(Color.BLACK);
	    		g2d.fill(brick[i]);
	    		g2d.draw(brick[i]);
	    	}
		}
		
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
	
	public void run() {
		while (true) {	
			if ((ball.x + MyBall.diameter) > MyCanvas.WIDTH || ball.x < 0) {
				ball.ballVelX *= -1;
			}
			
			if ((ball.y + MyBall.diameter) > MyCanvas.HEIGHT || ball.y < 0) {
				ball.ballVelY *= -1;
			}
			
			for (int i = 0; i < numBricks; i++) {
				if (brick[i].visible) {
					checkCollision(ball, brick[i]);
				}
			}
			
			ball.move();
			repaint();

			try
			{
				Thread.sleep(20);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}
	
	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));
		
		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
		
		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
			brick.visible = false;
		}
	}
}
