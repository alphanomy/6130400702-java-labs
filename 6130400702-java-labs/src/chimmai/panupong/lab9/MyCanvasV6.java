package chimmai.panupong.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import chimmai.panupong.lab8.MyBall;
import chimmai.panupong.lab8.MyCanvas;

/**
 * Is a class that paints a ball then move the ball 
 * along the screen. If the ball hits 
 * the edge of the screen, it will bounce back.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
	MyBallV2 ball;
	Thread running2;
	
	public MyCanvasV6() {
		ball = new MyBallV2((MyCanvas.WIDTH / 2) - (MyBall.diameter / 2), (MyCanvas.HEIGHT / 2) - (MyBall.diameter / 2));
		ball.ballVelX = 1;
		ball.ballVelY = 1;	
		running = new Thread(this);
		running.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		
		//Draw a ball
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
	
	public void run() {
		while (true) {	
			if ((ball.x + MyBall.diameter) >= MyCanvas.WIDTH || ball.x <= 0) {
				ball.ballVelX *= -1;
			}
			
			if ((ball.y + MyBall.diameter) >= MyCanvas.HEIGHT || ball.y <= 0) {
				ball.ballVelY *= -1;
			}
			
			ball.move();
			repaint();

			try
			{
				Thread.sleep(10);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}
}
