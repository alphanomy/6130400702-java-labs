package chimmai.panupong.lab9;

import chimmai.panupong.lab8.MyBall;

/**
 * Is a class that is intended to create 
 * a ball object with a radius of 15 units
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyBallV2 extends MyBall {
	protected int ballVelX, ballVelY;

	public MyBallV2(int x, int y) {
		super(x, y);
	}
	
	public void move() {
		x += ballVelX;
		y += ballVelY;	
	}

}
