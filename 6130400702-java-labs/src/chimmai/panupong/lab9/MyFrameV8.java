package chimmai.panupong.lab9;

import javax.swing.SwingUtilities;

/**
 * Java program that brings the components in class MyCanvasV8
 * to display on the screen.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyFrameV8 extends MyFrameV7 {
	public MyFrameV8(String text) {
		super(text);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV8 msw = new MyFrameV8("My Frame V8");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV8());
	}

}
