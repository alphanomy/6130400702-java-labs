package chimmai.panupong.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import chimmai.panupong.lab8.MyBall;
import chimmai.panupong.lab8.MyCanvas;
import chimmai.panupong.lab8.MyCanvasV4;

/**
 * Is a class that paints a ball at the middle of the left
 * then move the ball to the right of the screen 
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvasV5 extends MyCanvasV4 implements Runnable {

	protected MyBallV2 ball = new MyBallV2(0, (MyCanvas.HEIGHT / 2) - (MyBall.diameter / 2));
	protected Thread running = new Thread(this);
	
	public MyCanvasV5() {
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		
		//Draw a ball
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
	
	public void run() {
		while (true) {
		//Stop the ball
			if ((ball.x + MyBall.diameter) >= MyCanvas.WIDTH) {
				break;
			}
			
		//move the ball
			ball.move();	
		
		repaint();
		
		//Delay
			try
			{
				Thread.sleep(10);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}
	
}
