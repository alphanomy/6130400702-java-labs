package chimmai.panupong.lab9;

import chimmai.panupong.lab8.MyCanvas;
import chimmai.panupong.lab8.MyPedal;

/**
 * Is a class that is intended to create 
 * a pedal object with a width of 80 units
 * and a height of 20 units
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyPedalV2 extends MyPedal {
	protected final static int speedPedal = 20;
	
	public MyPedalV2(int x, int y) {
		super(x, y);
	}
	
	public void moveLeft() {
		if (this.x - speedPedal < 0) {
			this.x = 0;
		} else {
			this.x -= speedPedal;
		}
	}
	
	public void moveRight() {
		if (this.x + speedPedal > MyCanvas.WIDTH) {
			this.x = MyCanvas.WIDTH - MyPedal.pedalWidth;
		} else {
			this.x += speedPedal;
		}
	}
}
