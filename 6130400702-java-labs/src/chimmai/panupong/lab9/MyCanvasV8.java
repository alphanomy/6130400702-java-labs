package chimmai.panupong.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import chimmai.panupong.lab8.MyBall;
import chimmai.panupong.lab8.MyBrick;
import chimmai.panupong.lab8.MyCanvas;
import chimmai.panupong.lab8.MyPedal;

/**
 * This java program is a bouncing ball game.
 * Is a class that paints a ball , a pedal and 70 bricks 
 * the ball will move along the screen. If the ball hits 
 * the edge of the screen or brick,
 * that brick will invisible and the ball will bounce back.
 * If the ball passes below the screen 3 times, the game will over
 * and if the ball hits all of the bricks, you'll won this game.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-10
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {
	protected int numCol = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks;
	protected Thread running;
	protected Color[] color = {Color.MAGENTA, Color.BLUE, Color.BLUE, Color.CYAN, Color.GREEN,
			Color.YELLOW, Color.ORANGE, Color.RED};
	protected MyPedalV2 pedal;
	protected int lives;
	
	public MyCanvasV8() {
		bricks = new MyBrickV2[numRow][numCol];
		
		ball = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.diameter / 2, MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
		ball.ballVelX = 0;
		ball.ballVelY = 0;
		
		running = new Thread(this);
		
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, super.HEIGHT / 3 - i * MyBrick.brickHeight);
			}
		}
		
		running.start();
		setFocusable(true);
		addKeyListener(this);
		
		pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.pedalWidth / 2, MyCanvas.HEIGHT - MyPedal.pedalHeight);
		lives = 3;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]);	g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(4));
					g2d.setColor(Color.BLACK);	g2d.draw(bricks[i][j]);
				}
			}
		}
		
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
		
		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.BLUE);
		String s = " Lives : " + lives;
		g2d.drawString(s, 20, 30);
		
		if (numVisibleBricks == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 80));
			g2d.setColor(Color.GREEN);
			String won = "YOU WON";
			g2d.drawString(won, MyCanvas.WIDTH / 4, MyCanvas.HEIGHT / 2);
		}
		if (lives == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 80));
			g2d.setColor(Color.DARK_GRAY);
			String lose = "GAME OVER";
			g2d.drawString(lose, MyCanvas.WIDTH / 5, MyCanvas.HEIGHT / 2);
		}
	}
	
	private void checkPassBottom(MyBallV2 ball, MyPedalV2 pedal) {
		if (ball.y >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + (MyPedal.pedalWidth / 2)- (MyBall.diameter / 2);
			ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;
			lives--;
			repaint();
		}
	}
	
	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));
		
		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
		
		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
			brick.visible = false;
		}
	}
	
	private void checkCollideWithPedal(MyBallV2 ball, MyPedalV2 pedal) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedalV2.pedalWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyPedalV2.pedalHeight));
		
		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
		
		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
		}
	}
	
	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == KeyEvent.VK_LEFT) {
			pedal.moveLeft();
		} else if (arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
			pedal.moveRight();
		} else if (arg0.getKeyCode() == KeyEvent.VK_SPACE) {
			ball.ballVelY = 4;
			ball.ballVelX = (int)(Math.random() * 9) - 4;
		}
	}
	
	public void run() {
		while (true) {	
			if ((ball.x + MyBall.diameter) > MyCanvas.WIDTH || ball.x < 0) {
				ball.ballVelX *= -1;
			}
			
			if (ball.y < 0) {
				ball.ballVelY *= -1;
			}
			
			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkCollision(ball, bricks[i][j]);
					}
				}
			}
			
			checkCollideWithPedal(ball, pedal);
			checkPassBottom(ball, pedal);
			
			ball.move();
			repaint();

			try
			{
				Thread.sleep(30);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}








