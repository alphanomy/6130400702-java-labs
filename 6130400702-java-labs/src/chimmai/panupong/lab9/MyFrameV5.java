package chimmai.panupong.lab9;

import javax.swing.SwingUtilities;

import chimmai.panupong.lab8.MyFrameV4;

/**
 * Java program that brings the components in class MyCanvasV5
 * to display on the screen.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyFrameV5 extends MyFrameV4 {

	public MyFrameV5(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV5());
	}

}
