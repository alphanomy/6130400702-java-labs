package chimmai.panupong.lab5;

/**
 * AndroidSmartWatch is to model a android smart watch device object with
 * attributes model name, brand name, price and OS.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-02-18
 *
 */

public class AndroidSmartWatch extends AndroidDevice {
	private String modeName;
	private String brandName;
	private int price;
	
	/**
	 * Override method usage() from AndroidDevice
	 * to tell the usage of a android smart watch.
	 */
	public void usage() {
		System.out.println("AndroidSmartWatch Usage: Show tine, date, your heart rate and your step count.");
	}
	
	/**
	 * constructor method of a android smart watch
	 * that accepts 3 parameters:
	 * 
	 * @param modeName Model name
	 * @param brandName Brand name
	 * @param price Price
	 */
	public AndroidSmartWatch(String modeName, String brandName, int price) {
		super();
		this.modeName = modeName;
		this.brandName = brandName;
		this.price = price;
	}

	public String getModeName() {
		return modeName;
	}
	public void setModeName(String modeName) {
		this.modeName = modeName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	/**
	 * Override method toString() to be
	 * in format: "AndroidSmartWatch [Brand name:brandName
	 * 			, Model name:modeName, Price:price Baht]"
	 */
	public String toString() {
		return "AndroidSmartWatch [Brand name:" + brandName + ", Model name:" 
				+ modeName + ", Price:" + price + " Baht]";
	}
	
	/**
	 * It is a method that indicates that this device 
	 * can display only digital times.
	 */
	public void displayTime() {
		System.out.println("Display time only using a digital format.");
	}

}
