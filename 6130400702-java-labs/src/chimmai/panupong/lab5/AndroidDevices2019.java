package chimmai.panupong.lab5;

public class AndroidDevices2019 {

	public static void main(String[] args) {
		AndroidDevice ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
		AndroidDevice xiaomiMiBand3 = new AndroidSmartWatch("Xiaomi", "Mi Band 3", 950);
		ticwatchPro.usage();
		System.out.println(ticwatchPro);
		System.out.println(xiaomiMiBand3);

	}

}
