package chimmai.panupong.lab5;

/**
 * AndroidDevice is a abstract class for a android device
 * that have abstract method and a constant variable is OS.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-02-18
 *
 */

public abstract class AndroidDevice {
	
	/**
	 * Abstract method to tell the usage 
	 * of various android devices
	 */
	public abstract void usage();
	
	/**
	 * Set a OS variable which is a constant 
	 * with a value equal to "Android"
	 */
	private static final String OS = "Android";

	public static String getOs() {
		return OS;
	}
}
