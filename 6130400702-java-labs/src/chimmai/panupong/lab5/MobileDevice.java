package chimmai.panupong.lab5;

/**
 * MobileDevice is to model a mobile device object with
 * attributes model name, OS, price and weight.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-02-18
 *
 */

public class MobileDevice {
	private String modeName;
	private String os;
	private int price;
	private double weight;
	
	/**
	 * constructor method of a mobile device
	 * that accepts 4 parameters:
	 * 
	 * @param modeName Model name
	 * @param os Operating system
	 * @param price Price
	 * @param weight Body weight
	 */
	public MobileDevice(String modeName, String os, int price, double weight) {
		this.modeName = modeName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}
	
	/**
	 * constructor method of a mobile device
	 * that accepts 3 parameters:
	 * 
	 * @param modeName Model name
	 * @param os Operating system
	 * @param price Price
	 */
	public MobileDevice(String modeName, String os, int price) {
		this.modeName = modeName;
		this.os = os;
		this.price = price;
	}
	
	/**
	 * Override method toString() to be
	 * in format: "MobileDevice [Model name: modeName
	 * 			, OS:os, Price:price Baht, Weight:weight g]"
	 */
	public String toString() {
		return  "MobileDevice [Model name:" + modeName + ", OS:" + os 
				+ ", Price:" + price + " Baht, Weight:" + weight + " g]"; 
	}
	
	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
}
