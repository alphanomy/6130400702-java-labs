package chimmai.panupong.lab5;

/**
 * SamsungDevice is to model a Samsung device object with
 * attributes model name, price, weight and android version.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-02-18
 *
 */

public class SamsungDevice extends MobileDevice {
	
	private static String brand = "Samsung";
	private double androidVersion;
	
	/**
	 * constructor method of a Samsung device
	 * that accept 3 parameters:
	 * 
	 * @param modeName Model name
	 * @param price Price
	 * @param androidVersion Android version
	 */
	public SamsungDevice(String modeName, int price, double androidVersion) {
		super(modeName, "Android", price);
		this.androidVersion = androidVersion;
	}
	
	/**
	 * constructor method of a Samsung device
	 * that accepts 4 parameters:
	 * 
	 * @param modeName Model name
	 * @param price Price
	 * @param weight Body weight
	 * @param androidVersion Android version
	 */
	public SamsungDevice(String modeName, int price, double weight, double androidVersion) {
		super(modeName, "Android", price, weight);
		this.androidVersion = androidVersion;
	}
	
	public static String getBrand() {
		return brand;
	}

	public double getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}
	
	/**
	 * Override method toString() to be
	 * in format: "SamesungDevice [Model name:modeName
	 * 			, OS:os, Price:price Baht, Weight:weight g, Android version:androidVersion]"
	 */
	public String toString() {
		return  "SamsungDevice [Model name:" + getModeName() + ", OS:" + getOs() 
				+ ", Price:" + getPrice() + " Baht, Weight:" + getWeight() 
				+ " g, Android version:" + androidVersion + "]";
		
	}
	
	/**
	 * It is a method that indicates that this device 
	 * can display both digital and analog times.
	 */
	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch.");
	}

}
