package chimmai.panupong.lab7;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV6 is a program to display a simple GUI.
 * The program extends from MobileDeviceFormV5 so this will 
 * display like MobileDeviceFormV5.
 * But only adding a Galaxy Note 9 into the window
 * which is between review message box and cancel-OK buttons. 
 * 
 * @author Panupong Chimmai
 * ID: 613040070-2 
 * Sec: 2
 * @since 2019-03-04
 * @version 1.0
 * 
 */

class ImagePanel extends JPanel {
    protected BufferedImage img;
    public ImagePanel() {
		try {
			img = ImageIO.read(new File("images/galaxyNote9.jpg"));
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}
    
    public Dimension getPreferredSize() {
			return new Dimension(img.getWidth(),
					img.getHeight());
	}
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, this);           
    }

}

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
	
	public MobileDeviceFormV6(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();
		ImagePanel imgPanel = new ImagePanel();
		//imgPanel.setPreferredSize();
		reviewPanel.remove(revScroll);
		reviewPanel.add(revScroll, BorderLayout.CENTER);
		reviewPanel.add(imgPanel, BorderLayout.SOUTH);
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV6 mobileDeviceForm6 = new MobileDeviceFormV6("Mobile Device Form V6");
		mobileDeviceForm6.addComponents();
		mobileDeviceForm6.addMenus();
		mobileDeviceForm6.setFrameFeatures();
		}

		public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}

	}
