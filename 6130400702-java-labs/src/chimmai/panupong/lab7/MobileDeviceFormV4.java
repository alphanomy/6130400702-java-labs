package chimmai.panupong.lab7;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import chimmai.panupong.lab6.MobileDeviceFormV3;

/**
 * This java program MobileDeviceFormV4 is a program to display a simple GUI.
 * The program extends from MobileDeviceFormV3 so this will 
 * display like MobileDeviceFormV3.
 * But only adding a icon picture to "File" menu
 * and adding sub-menu items to "Color" and "Size" menu.
 * 
 * 
 * @author Panupong Chimmai
 * ID: 613040070-2 
 * Sec: 2
 * @since 2019-03-04
 * @version 1.0
 * 
 */

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	protected ImageIcon newIcon;
	protected JMenuItem redMI, greenMI, blueMI, s16MI, s20MI ,s24MI;
	
	public MobileDeviceFormV4(String title) {
		super(title);
	}
	
	protected void updateMenuIcon() {
		newIcon = new ImageIcon("images/new.jpg");
		newMI.setIcon(newIcon);
	}
	
	protected void addSubMenu() {
		confMenu.remove(colorMI);
		confMenu.remove(sizeMI);
		redMI = new JMenuItem("Red");
		greenMI = new JMenuItem("Green");
		blueMI = new JMenuItem("Blue");
		colorMI = new JMenu("Color");
		colorMI.add(redMI);
		colorMI.add(greenMI);
		colorMI.add(blueMI);
		s16MI = new JMenuItem("16");
		s20MI = new JMenuItem("20");
		s24MI = new JMenuItem("24");
		sizeMI = new JMenu("Size");
		sizeMI.add(s16MI);
		sizeMI.add(s20MI);
		sizeMI.add(s24MI);
		confMenu.add(colorMI);
		confMenu.add(sizeMI);
	}
	
	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenu();
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceForm4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceForm4.addComponents();
		mobileDeviceForm4.addMenus();
		mobileDeviceForm4.setFrameFeatures();
		}

		public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}

	}