package chimmai.panupong.lab7;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV5 is a program to display a simple GUI.
 * The program extends from MobileDeviceFormV4 so this will 
 * display like MobileDeviceFormV4.
 * But change font type and size of labels to bold Serif font type and size 14,
 * change font type and size of message in the boxes to
 * plain Serif font type and size 14 and change text color of
 * OK button to blue and Cancel button to red.
 * 
 * 
 * @author Panupong Chimmai
 * ID: 613040070-2 
 * Sec: 2
 * @since 2019-03-04
 * @version 1.0
 * 
 */

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {
	protected Font labelFont, txtFiFont;
	
	
	public MobileDeviceFormV5(String title) {
		super(title);
	}
	
	protected void initComponents() {
		labelFont = new Font("Serif", Font.PLAIN,14);
		txtFiFont = new Font("Serif", Font.BOLD,14);
		
		brandLabel.setFont(labelFont);
		modelLabel.setFont(labelFont);
		weightLabel.setFont(labelFont);
		priceLabel.setFont(labelFont);
		osLabel.setFont(labelFont);
		typeLabel.setFont(labelFont);
		featLabel.setFont(labelFont);
		revLabel.setFont(labelFont);
		
		brandTxtField.setFont(txtFiFont);
		modelTxtField.setFont(txtFiFont);
		weightTxtField.setFont(txtFiFont);
		priceTxtField.setFont(txtFiFont);
		revTxtArea.setFont(txtFiFont);
		
		okButton.setForeground(Color.BLUE);
		cancelButton.setForeground(Color.RED);
	}

	
	protected void addComponents() {
		super.addComponents();
		initComponents();
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceForm5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceForm5.addComponents();
		mobileDeviceForm5.addMenus();
		mobileDeviceForm5.setFrameFeatures();
		}

		public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}

	}