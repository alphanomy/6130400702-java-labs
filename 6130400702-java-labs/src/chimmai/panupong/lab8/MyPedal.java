package chimmai.panupong.lab8;

import java.awt.geom.Rectangle2D;

/**
 * Is a class that is intended to create 
 * a rectangle object with a with a width of 80 units
 * and a height of 20 units
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyPedal extends Rectangle2D.Double {
	protected final static int pedalWidth = 100;
	protected final static int pedalHeight = 10;
	
	public MyPedal(int x, int y) {
		super(x, y, pedalWidth, pedalHeight);
	}
}