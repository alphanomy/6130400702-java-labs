package chimmai.panupong.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * Is a class that paints: balls in the 4 corners of the screen,
 * and 10 bricks placed in the middle of the screen.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvasV3 extends MyCanvasV2 {
	protected MyBall[] ballArray = new MyBall[4];
	protected MyBrick[] brickArray = new MyBrick[10];
	protected int diameter = MyBall.diameter;
	protected int brickWidth = MyBrick.brickWidth;
	protected int brickHeight = MyBrick.brickHeight;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
	    g2d.setColor(Color.BLACK);
	    g2d.fillRect(0, 0, WIDTH, HEIGHT);
	    
	    ballArray[0] = new MyBall(0, 0);
	    ballArray[1] = new MyBall(WIDTH - diameter, 0);
	    ballArray[2] = new MyBall(0, HEIGHT - diameter);
	    ballArray[3] = new MyBall(WIDTH - diameter, HEIGHT - diameter);
	    
	    g2d.setColor(Color.WHITE);
	    for ( int i=0; i < ballArray.length; i++) {
	    	g2d.fill(ballArray[i]);
	    }
	    
	    for ( int i = 0; i < brickArray.length; i++) {
	    	int x = 0 + (i * brickWidth);
	    	int y = (HEIGHT / 2) - (brickHeight / 2);
	    	
	    	brickArray[i] = new MyBrick(x, y);
	    	g2d.setColor(Color.WHITE);
	    	g2d.fill(brickArray[i]);
	    	
	    	g2d.setColor(Color.BLACK);
	    	g2d.setStroke(new BasicStroke(3));
	    	g2d.draw(brickArray[i]);
	    }
	    
	}
	
}
