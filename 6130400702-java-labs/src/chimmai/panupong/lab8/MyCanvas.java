package chimmai.panupong.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

/**
 * Is a class that paints a person's face by Graphics2D
 * which consists of a face frame, two eyes and one mouth. 
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvas extends JPanel {
	protected final int WIDTH = 800;
	protected final int HEIGHT = 600;

	
	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setBackground(Color.BLACK);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int halfWidth = WIDTH / 2;
		int halfHeight = HEIGHT / 2;
		
		Ellipse2D.Double circle = new Ellipse2D.Double(halfWidth - 150, halfHeight - 150, 300, 300);
		Ellipse2D.Double ovalLeft = new Ellipse2D.Double(halfWidth - 50, halfHeight - 50, 30, 60);
		Ellipse2D.Double ovalRight = new Ellipse2D.Double(halfWidth + 20, halfHeight - 50, 30, 60);
		Rectangle2D.Double square = new Rectangle2D.Double(halfWidth - 50, halfHeight + 70, 100, 10);
		
	    Graphics2D g2d = (Graphics2D)g;
	    g2d.setColor(Color.WHITE);
	    g2d.draw(circle);
	    g2d.fill(ovalLeft);
	    g2d.fill(ovalRight);
	    g2d.fill(square);
	    
	  }
}
