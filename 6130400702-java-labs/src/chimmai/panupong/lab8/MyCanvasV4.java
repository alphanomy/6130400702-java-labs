package chimmai.panupong.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * Is a class that paints: 10 bricks arranged in 7 rows, 
 * each row has different colors, by painting at the top of the screen
 * and a pedal with the ball placed above at the bottom of the screen.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvasV4 extends MyCanvasV3 {
	protected MyBrick[] brickArray;
	protected MyBall ball;
	protected MyPedal pedal;
	protected int pedalWidth = MyPedal.pedalWidth;
	protected int pedalHeight = MyPedal.pedalHeight;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
	    g2d.setColor(Color.BLACK);
	    g2d.fillRect(0, 0, WIDTH, HEIGHT);
	    
		Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED };
	    
	    brickArray = new MyBrick[70];
	    int row = 7;
	    for ( int i = 0; i < row; i++ ) {
	    	for ( int j = 0; j < (brickArray.length / row); j++ ) {
		    	int x = 0 + (j * brickWidth);
		    	int y = (HEIGHT / 3) - (i * brickHeight);
		    	
		    	brickArray[j] = new MyBrick(x, y);
		    	g2d.setColor(color[i]);
		    	g2d.fill(brickArray[j]);
		    	
		    	g2d.setColor(Color.BLACK);
		    	g2d.setStroke(new BasicStroke(3));
		    	g2d.draw(brickArray[j]);
	    	}
	    }
	    
	    pedal = new MyPedal((WIDTH / 2) - (pedalWidth / 2), HEIGHT - pedalHeight);
	    g2d.setColor(Color.GRAY);
    	g2d.fill(pedal);
    	
    	ball = new MyBall((WIDTH / 2) - (diameter / 2), HEIGHT - (diameter + pedalHeight));
    	g2d.setColor(Color.WHITE);
    	g2d.fill(ball);
    	
	}
}
