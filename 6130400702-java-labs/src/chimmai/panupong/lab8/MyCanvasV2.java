package chimmai.panupong.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * Is a class that paints: a brick in the middle of the top,
 * a ball in the middle, a pedal in the middle of the bottom
 * and straight lines drawn through the middle, both vertically and horizontally.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyCanvasV2 extends MyCanvas {
	protected MyBall ball;
	protected MyBrick brick;
	protected MyPedal pedal;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
	    g2d.setColor(Color.BLACK);
	    g2d.fillRect(0, 0, WIDTH, HEIGHT);
	    
	    g2d.setColor(Color.WHITE);
	    ball = new MyBall((WIDTH / 2) - 15, (HEIGHT / 2) - 15);
		brick = new MyBrick((WIDTH / 2) - 40, 0);
		pedal = new MyPedal((WIDTH / 2) - 50, HEIGHT - 10);
	    g2d.fill(ball);
	    g2d.fill(brick);
	    g2d.fill(pedal);
	    
	    g2d.drawLine(WIDTH / 2, 0, WIDTH / 2, HEIGHT);
	    g2d.drawLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
	}
}
