package chimmai.panupong.lab8;

import javax.swing.SwingUtilities;

/**
 * Java program that brings the components in class MyCanvasV2 
 * to display on the screen.
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyFrameV2 extends MyFrame {

	public MyFrameV2(String text) {
		super(text);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV2());
	}

}
