package chimmai.panupong.lab8;

import java.awt.geom.Ellipse2D;

/**
 * Is a class that is intended to create 
 * a ball object with a radius of 15 units
 * 
 * @author Panupong Chimmai
 * @version 1.0
 * @since 2019-04-03
 * ID: 613040070-2
 * Sec: 2
 */

public class MyBall extends Ellipse2D.Double {
	protected final static int diameter = 30;

	public MyBall(int x, int y) {
		super(x, y, diameter, diameter);
	}
}